Submit your pull requests here to be included on https://koha-community.org/about/libraries 

Additionally, if you are a library that uses Koha, you should consider enabling reporting of your installation automatically to http://hea.koha-community.org/